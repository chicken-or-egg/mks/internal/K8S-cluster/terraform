provider "selectel" {
  token = var.my_selectel_token
}


resource "selectel_vpc_project_v2" "my-k8s" {
  name = "my-k8s-cluster"
  theme = {
    color = "269926"
  }
//  auto_quotas = true
  quotas {
    resource_name = "compute_cores"
    resource_quotas {
      region = var.region
      zone = "${var.region}a"
      value = 16
    }
  }
  quotas {
    resource_name = "network_floatingips"
    resource_quotas {
      region = var.region
      value = 1
    }
  }
  quotas {
    resource_name = "load_balancers"
    resource_quotas {
      region = var.region
      value = 1
    }
  }
  quotas {
    resource_name = "compute_ram"
    resource_quotas {
      region = var.region
      zone = "${var.region}a"
      value = 32768
    }
  }
  quotas {
    resource_name = "volume_gigabytes_fast"
    resource_quotas {
      region = var.region
      zone = "${var.region}a"
      # (20 * 2) + 50 + (8 * 3 + 10)
      value = 130
    }
  }
}

resource "selectel_mks_cluster_v1" "k8s-cluster" {
  name         = "k8s-cluster"
  project_id   = selectel_vpc_project_v2.my-k8s.id
  region       = var.region
  kube_version = "1.17.9"
}

resource "selectel_mks_nodegroup_v1" "nodegroup_1" {
  cluster_id        = selectel_mks_cluster_v1.k8s-cluster.id
  project_id        = selectel_mks_cluster_v1.k8s-cluster.project_id
  region            = selectel_mks_cluster_v1.k8s-cluster.region
  availability_zone = "${var.region}a"
  nodes_count       = 2
  cpus              = 8
  ram_mb            = 16384
  volume_gb         = 15
  volume_type       = "fast.${var.region}a"
  labels            = {
    "project": "chicken-or-egg",
  }
}

