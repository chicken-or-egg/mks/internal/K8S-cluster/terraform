output "project_id" {
  value = selectel_vpc_project_v2.my-k8s.id
}

output "k8s_id" {
  value = selectel_mks_cluster_v1.k8s-cluster.id
}

output "user_name" {
  value = selectel_vpc_user_v2.my-k8s-user.name
}

output "user_pass" {
  value = selectel_vpc_user_v2.my-k8s-user.password
}

output "quotas" {
  value = selectel_vpc_project_v2.my-k8s.all_quotas
}
