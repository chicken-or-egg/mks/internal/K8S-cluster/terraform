resource "random_password" "my-k8s-user-pass" {
  length = 16
  special = true
  override_special = "_%@"
}

resource "selectel_vpc_user_v2" "my-k8s-user" {
  password = random_password.my-k8s-user-pass.result
  name = var.username
  enabled  = true
}

resource "selectel_vpc_keypair_v2" "my-k8s-user-ssh" {
  public_key = file("~/.ssh/id_rsa.pub")
  user_id    = selectel_vpc_user_v2.my-k8s-user.id
  name = var.username
}

resource "selectel_vpc_role_v2" "my-k8s-role" {
  project_id = selectel_vpc_project_v2.my-k8s.id
  user_id    = selectel_vpc_user_v2.my-k8s-user.id
}
